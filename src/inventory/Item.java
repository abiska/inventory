package inventory;


/**
 *
 * @author iuabd
 */
public class Item {
    
    //attributes
    private int itemID;
    private String name;
    private int quantity;

    //constructor(s)
    public Item(){}
    public Item(int itemID, String name, int quantity) {
        this.itemID = itemID;
        this.name = name;
        this.quantity = quantity;
    }
    
    //getters
    public int getItemID() {
        return itemID;
    }
    public String getName() {
        return name;
    }
    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "\nItem ID=" + getItemID() + "\nName=" + getName() + "\nQuantity=" + getQuantity();
    }
    
    
}
