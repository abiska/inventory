package inventory;

import java.util.ArrayList;

/**
 *
 * @author iuabd
 */
public class InventorySimulation {

    public static void main(String[] args) {
        
        Inventory inv = new Inventory();
        
        Item item1 = new Item(1, "ABC", 2);
        inv.addItem(item1);
        
        Item item2 = new Item(3, "DEF", 4);
        inv.addItem(item2);
        
        
        
        
//        ArrayList<Item> items = new ArrayList<>();
//        items.add(item1);
//        items.add(item2);
        
        
       
       
        inv.printInventory();
    
}
}
